package org.rygn.kanban.controller;

import org.rygn.kanban.dao.DeveloperRepository;
import org.rygn.kanban.dao.TaskRepository;
import org.rygn.kanban.domain.Developer;
import org.rygn.kanban.domain.Task;
import org.rygn.kanban.service.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class DeveloperController {


    @Autowired
    private DeveloperService developerService;

    @GetMapping(path = "/developers", produces = "application/json")
    public Collection<Developer> findAllDevelopers() {

        return developerService.findAllDevelopers();
    }


}
