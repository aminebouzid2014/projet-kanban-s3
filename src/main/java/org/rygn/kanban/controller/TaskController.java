package org.rygn.kanban.controller;


import org.rygn.kanban.dao.TaskRepository;
import org.rygn.kanban.domain.Task;
import org.rygn.kanban.domain.TaskStatus;
import org.rygn.kanban.service.TaskService;
import org.rygn.kanban.service.impl.TaskServiceImpl;
import org.rygn.kanban.utils.Constants;
import org.rygn.kanban.utils.TaskMoveAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class TaskController {

    @Autowired
    private TaskService taskService;

    @GetMapping(path = "/tasks", produces ="application/json")
    public Collection<Task> findAllTasks() {
        return taskService.findAllTasks();
    }


    @PostMapping(path ="/tasks")
    public Task addTasks(@Validated @RequestBody Task task){
        return this.taskService.createTask(task);
    }

    @PatchMapping("/tasks/{id}")
    public Task moveTask(@RequestBody String side, @PathVariable Long id) {

        Task task = this.taskService.findTask(id);
        switch(side) {
            case Constants.MOVE_RIGHT_ACTION:
                taskService.moveRightTask(task);
                return task;
            case Constants.MOVE_LEFT_ACTION:
                taskService.moveLeftTask(task);
                return task;
            default:
                System.out.println("Error cannot change taskstatus");
                return task;
        }

    }




}
